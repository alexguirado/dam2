package iesam;

import java.io.Serializable;
/* Crea la classe Persona que té:
 * - Atributs: nom, edat
 * - Mètodes: constructor, complirAnys(), toString()
 * D'aquesta classe derivaran les classes Alumne i Professor
 */

public class Persona implements Serializable {

	private String nom;
	private int edat;

	public Persona(String nom, int edat) {
		this.nom = nom;
		this.edat = edat;
	}

	public Persona() {
	}

	public void complirAnys() {
		edat = edat + 1;
	}

	public String toString() {
		return ("Nom: " + nom + "   Edat: " + edat);
	}

	public void imprimir() {
		System.out.println( this );
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

//	@Override
//	public boolean menorQue(Ordenable a) {
//		Persona p = (Persona) a;
//		if(this.edat < p.edat) return true;
//		return false;
//	}

}
