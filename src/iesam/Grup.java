package iesam;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Vector;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

@SuppressWarnings("serial")
public class Grup implements Serializable {

	Vector<Alumne> alumnes = new Vector<Alumne>();

	// Constructor
	public Grup() {

		int n; // número d'alumnes
		String nom; // nom d'un alumne
		float nota; // nota d'un alumne
		int edat; // edat d'un alumne

		Scanner teclat = new Scanner(System.in);

		// Demana quants alumnes volem (comprovant que el valor és correcte)
		System.out.println("Quants alumnes vols? ");
		do {
			n = teclat.nextInt();
		} while (n <= 0);

		alumnes = new Vector<Alumne>(n);

		for (int i = 0; i < alumnes.capacity(); i++) {

			System.out.println("Nom de l'alumne " + (i + 1) + " : ");
			nom = teclat.next();
			do {
				System.out.println("Edat de l'alumne " + (i + 1) + " : ");
				edat = teclat.nextInt();
			} while (edat <= 0);
			do {
				System.out.println("Nota de l'alumne " + (i + 1) + " : ");
				nota = teclat.nextFloat();
			} while ((nota < 0) || (nota > 10));
			alumnes.add(new Alumne(nom, edat, nota));
		}
	};

	public int numAprovats() {
		Iterator<Alumne> it = alumnes.iterator();
		int n = 0;
		while (it.hasNext()) {
			if (it.next().getNota() >= 5) {
				n++;
			}

		}
		return n;
	};

	public void imprimir() {
		Iterator<Alumne> it = alumnes.iterator();
		while (it.hasNext()) {
			it.next().imprimir();
		}
	}

	@Override
	public String toString() {
		return "Grup [alumnes=" + alumnes + "]";
	}

	public static void main(String[] args) {
		// Grup test = new Grup();
		Alumne a1 = new Alumne("Alex", 32, 10);
		Persona per1 = new Persona("Alex", 32);
		Professor pro1 = new Professor("Oriol", 99, 500000);
		
		// Serializar
		
		Gson gson = new Gson();
		
		String jsonOutput = gson.toJson(a1);
		System.out.println(jsonOutput);

		try {
			FileWriter writer = new FileWriter("alumne.json");
			writer.write(jsonOutput);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String jsonOutput2 = gson.toJson(pro1);
		System.out.println(jsonOutput2);

		try {
			FileWriter writer = new FileWriter("profesor.json");
			writer.write(jsonOutput2);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Deserializar
		Alumne a2 = new Alumne();
		a2 = gson.fromJson(jsonOutput, Alumne.class);
		System.out.println(a2);

		// JsonWriter writer;
		// try {
		// writer = new JsonWriter(new FileWriter("user.json"));
		// writer.beginObject();
		// writer.name("nom").value(p1.getNom());
		// writer.endObject();
		// writer.close();
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// }

		// test = gson.fromJson(jsonOutput, Grup.class);
		// System.out.println(test);

	}

}