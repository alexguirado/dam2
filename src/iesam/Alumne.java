package iesam;

import java.io.Serializable;

public class Alumne extends Persona implements Serializable {

	private float nota;
	Professor tutor; 

	public Professor getTutor() {
		return tutor;
	}

	public void setTutor(Professor tutor) {
		this.tutor = tutor;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public Alumne(String nom, int edat, float nota) {
		super(nom, edat);
		this.nota = nota;
	}

	public Alumne() {
	}

	public void complirAnys() {
		super.complirAnys();
		nota = 10;
	}

	public String toString() {
		return (super.toString() + "   Nota: " + nota);
	}
	

}